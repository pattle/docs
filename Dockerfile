FROM python:alpine AS build

WORKDIR /docs
COPY . .

RUN pip install mkdocs
# Use a specific version, only upgrade
# manually in case of css change
RUN pip install mkdocs-material==3.1.0
RUN mkdocs build

FROM nginx

COPY --from=build /docs/site /usr/share/nginx/html

EXPOSE 80
