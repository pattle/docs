# Authentication

When the user opens the app for the first time after install, the user sees 3 buttons,
which represent the 3 methods of authentication. In the future, when Matrix supports it

1. [Login with phone number](#1-phone-number)
2. [Login with email](#2-email)
3. [Login with username](#3-username)

For every method of authentication, the fact whether it's a *login* or a *registration*, is
determined whether their main identifier (phone number, email or username) exists on the
home server. If it doesn't it's a registration, otherwise, a login.

### Advanced

In every first stage of the authentication, when the identifier is specified, an *advanced* button
is display in the top right corner, in a semi-hidden way. This button opens a different overlay
screen where the user can specify their *home server* and *identity server*.

#### Home server

This option consists of a radio button with two options:

1. Default home server selection
   The default home server selection tries different when querying usernames (and logging in if
   found) home servers, in this order:
    1. 'https://pattle.im'
    2. 'https://matrix.org'
2. *A text box where the user can enter a home server url*.

#### Identity server

Text box with default value: 'https://pattle.im'

## 1. Phone number

The phone authentication button is the most prominent one, because this is what most
casual users are familiar with.

## 2. Email

## 3. Username

When choosing the third option, the user is expected to be a *bit* more knowledgeable about
Matrix and technology in general, however, everything is still made as easy as possible according
to the philosophy of Pattle.

The authentication flow for username authentication is as follows:

1. Username
2. Password
3. Avatar & display name (registration only)

### 1. Username input

A screen with one text input: the username. This can be specified in two formats:

1. A fully qualified Matrix ID
   A complete Matrix ID in the format of: `@[localpart]:[homeserver]`. Example:
   `@wilko:pattle.im`
   See [the Matrix spec](https://matrix.org/docs/spec/appendices.html#user-identifiers) for details.
   If this format is used, the [homeserver] will be used to login, and will also be set in the
   [advanced options](#advanced). This will override any setting present in the advanced options.
2. Just the local part of a Matrix ID
   This can only contain letters, numbers and `.`, `_`, `=`, `-`, `/`.
   
Offline check are done to make sure that the input is correct.