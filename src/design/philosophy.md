# Design philosophy

The goal of Pattle is to create an app that behaves similiary (and almost exactly) like
many modern popular chat apps of today, like WhatsApp, Telegram and Signal. This is not
the only goal, however: the primary goal of Pattle is to be a [Matrix](https://matrix.org)
app that *everyone* can use, in terms of simplicity.

## Differences between Pattle and standard Matrix clients

Pattle is different from other Matrix clients, in the sense that it tries to emulate existing,
*easy to use* apps that are more popular at the moment, which may have the result that sometimes
Pattle will behave differently from what's normally expected in Matrix.

*  Bridges to other protocols are a seamless experience.
*  Personal chats between two people are not a room of 2 people, but an actual personal chat.
   (However, Riot does attempt this, but doesn't hide it that well)
*  Group chats (Pattle terminology for rooms with less than 256 people) should be joined instantly
   when 'invited'. Note that this does not apply to public chats (chats with a public address
   **and** more than 256  people).
*  If someone quotes a part of a message, this is still clickable like a reply.
*  Permissions are simplified into two roles, admin and non-admins.